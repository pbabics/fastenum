pytest==6.2.4
pytest-benchmark==3.2.3
pytest-cov==2.10.0
pytest-mypy-plugins==1.9.3
ql-cq==0.42.0
ql-orange==1.3.0
# We have to pin this explicitly because pip is prone to picking up the unbounded 'mypy>=0.730' requirement
# from 'pytest-mypy-plugins' instead of the pin from ql-cq. Looking forward to the new pip resolver...
mypy==0.931
pre-commit==2.6.0
